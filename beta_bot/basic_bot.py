from discord.ext import commands


from ig_bot.protected.bot_token import test_token

description = '''An example and testing bot
Use for me to work on the loose concepts for the rework'''

bot = commands.Bot(command_prefix='?', description=description)


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('-------')


@bot.command()
async def test(ctx):
    await ctx.send("Hello there")




bot.run(test_token)
