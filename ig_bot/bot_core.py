#!/usr/bin/env python3

import aiohttp
from discord import Embed, Game
from discord.ext.commands import Bot, when_mentioned_or

from protected.bot_token import token

# from .pagination import bot_pagination

# import logging

# #  Logging for testing purposes
# logger = logging.getLogger('discord')
# logger.setLevel(logging.DEBUG)
# handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
# handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
# logger.addHandler(handler)

#  Constants for bot's usage
BASE_API_URL = "https://plaza.dsolver.ca/api/games"
EMBED_QUERY = '&customFields=slug,name,link,shortDescription,logo'
BASE_GAME_URL = 'https://plaza.dsolver.ca/games/'
MISSING_DESCRIPTION = "There's no description written for this game.  Why not help us out and write it for us after you try out the game?"
BASE_TAGS_URL = "https://plaza.dsolver.ca/api/tags"

bot = Bot(
    command_prefix='$',
    case_insensitive=True
)


@bot.event
async def on_ready():
    print(f'Logged in as')
    print(f'{bot.user.name}')
    print(f'{bot.user.id}')
    print('----------')
    await status_update()


@bot.command()
async def random_game(ctx, *args):
    """
    Suggests a random game from The Plaza.
    If you want to specify what game you want, add tags after the command.
    Any tags that are more than one word need to be in quotes.
    Example:  $random_game kongregate "resource management"
    """
    async with aiohttp.ClientSession() as session:
        if len(args) == 0:
            async with session.get(f'{BASE_API_URL}?random=true{EMBED_QUERY}') as resp:
                if resp.status != 200:
                    await ctx.send("Something went wrong!  Please try again in a few moments.")
                    return
                rand_game, = await resp.json()  # Comma after rand_game to get unpack the list and get the dict
            embed = rand_embed_maker(rand_game)
            await ctx.send(content=f'{ctx.author.mention}', embed=embed)
            return
        else:
            tags = '&tags=' + ','.join(args)
            tags = tags.replace(' ', '%20')
            async with session.get(f'{BASE_API_URL}?random=true{EMBED_QUERY}{tags}') as resp:
                if resp.status != 200:
                    await ctx.send("Something went wrong!  Please try again in a few moments.")
                    return
                try:
                    rand_game, = await resp.json()
                except ValueError:
                    await ctx.send(
                        f"{ctx.author.mention} It seems there were no games with the tags you provided.  Please use the {bot.command_prefix}tags command to see your options.")
                    return
            embed = rand_embed_maker(rand_game)
            await ctx.send(content=f'{ctx.author.mention}', embed=embed)
            return


@bot.command()
async def prefix_change(ctx, new_prefix=None):
    """
    Allows administrators to change the prefix used by the bot to help prevent conflicts with other bots
    :param ctx: The context of the command call.  Used internally by the bot to route messages to the correct place
    :param new_prefix: The desired new prefix to use for bot commands.  Can only be a single character
    :return: None
    """
    if ctx.author.guild_permissions.administrator:
        if new_prefix is None:
            await ctx.send("Hey, looks like you forgot to add the prefix you wanted to change to.")
            return
        elif len(new_prefix) > 1:
            await ctx.send("Your new prefix can only be a single character.  Please try again.")
            return
        else:
            bot.command_prefix = when_mentioned_or(new_prefix)
            await status_update()
            await ctx.send(f"Your new prefix has been set to ``{new_prefix}``")
            return
    else:
        await ctx.send(f"{ctx.author.mention} Sorry, you don't have the proper permissions to do that.")
        return


@bot.command()
async def tags(ctx, *args):
    """
    Allows a user to look up all the tags they can use to filter their random_game call with
    If no arguments are provided, it will print a guide on how to use the command
    If the argument "all" is provided, it will print all the categories and their tags
    If a category is provided, it will print all the tags from that category.  Can have more than one category argument.
    :param ctx: The context of the command call.  Used internally by the bot to route messages to the correct place
    :param args: Either the category name or "all" to print all tags
    :return: None
    """
    async with aiohttp.ClientSession() as session:
        async with session.get(f'{BASE_TAGS_URL}') as resp:
            if resp.status != 200:
                await ctx.send("Something went wrong!  Please try again in a few moments.")
                return
            tags_dict = await resp.json()
    if len(args) == 0:
        tag_embed = Embed(
            title="Tags:  A How-To",
            description=f"To view tags of a specific category, use the tags command followed by the category name.\nNote: You can use multiple category names to get more than one list of tags.\nTo view all tags, use the tags command followed by ``all``\nExamples:\n``{bot.command_prefix}tags platform``\n``{bot.command_prefix}tags all``"
        )
        tag_embed.add_field(
            name="Categories:",
            value="\n".join(tags_dict.keys())
        )
        await ctx.send(f'{ctx.author.mention}', embed=tag_embed)
        return
    elif "all" in args:
        tag_embed = Embed(
            title="Tags"
        )
        for category, tags in tags_dict.items():
            tag_embed.add_field(
                name=f"{category}:",
                value="\n".join(tags)
            )
        await ctx.send(f'{ctx.author.mention}', embed=tag_embed)
        return
    else:
        tag_embed = Embed(
            title="Tags:"
        )
        for category in args:
            if category.capitalize() not in tags_dict.keys():
                await ctx.send(
                    f"{ctx.author.mention} The category you tried to look up doesn't exist!  Please consult {bot.command_prefix}tags for the category list.")
                return
            tag_embed.add_field(
                name=f"{category.capitalize()}:",
                value="\n".join(tags_dict[category.capitalize()])
            )
        await ctx.send(f'{ctx.author.mention}', embed=tag_embed)
        return


async def status_update():
    await bot.change_presence(activity=Game(name=f'For help type {bot.command_prefix}help'))
    return


def rand_embed_maker(rand_game):
    embed = Embed(title=f"**{rand_game['name']}**",
                  url=f"{rand_game['link']}",
                  description=f"{rand_game.get('shortDescription', MISSING_DESCRIPTION)}"
                  )
    if rand_game.get('logo', None) is not None:
        embed.set_image(url=f"https://plaza.dsolver.ca/uploads/{rand_game['logo']}")
    embed.add_field(name="The Plaza",
                    value=f"""[Visit {rand_game['name']}'s Plaza Page!]({BASE_GAME_URL}{rand_game['slug']})
                    Make sure to review the game on the Plaza after you play it!""")
    return embed


bot.run(token)
