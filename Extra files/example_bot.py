import asyncio
import json
from random import choice

import aiohttp
import discord

from ig_bot.protected.bot_token import token


# Create class using discord.Client
class MyClient(discord.Client):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Creates background task and runs it
        self.bg_task = self.loop.create_task(self.game_list_get())
        self.game_list = None
        self.prefix = "$"

    async def on_ready(self):
        print('Logged in as')
        print(self.user.name)
        print(self.user.id)
        print('------')

    async def game_list_get(self):
        print("Getting ready")
        await self.wait_until_ready()
        print("Ready")
        while not self.is_closed():
            print("In While loop")
            async with aiohttp.ClientSession() as session:
                print("In session with")
                async with session.get('https://plaza.dsolver.ca/api/games') as resp:
                    print("In resp with")
                    self.game_list = await resp.text()
                    print("Game List Updated")
            print("sleepy time")
            await asyncio.sleep(3600)


client = MyClient()


# async def game_list_get():
#     async with aiohttp.ClientSession() as session:
#         async with session.get('https://plaza.dsolver.ca/api/games') as resp:
#
#             with open('plaza_list.json', 'w') as file:
#                 json.dump(await resp.json(), file)
#                 print("JSON Imported")


# @client.event
# async def on_ready():
#     print(f'We have logged in as {client.user}')


@client.event
async def on_message(message):
    if message.author == client.user or message.author.bot:
        return

    if message.content.startswith(f'{client.prefix}hello'):
        await message.channel.send('Hello!')

    # if message.content.startswith('$gamelist'):
    #     await message.channel.send(f'I got the game list, {message.author.mention}.')

    if message.content.lower().startswith(f'{client.prefix}myname'):
        await message.channel.send(f'Your name is {message.author.mention}.')

    if message.content.startswith(f'{client.prefix}random_game'):
        game = choice(json.loads(client.game_list))
        await message.channel.send(f'{message.author.mention} Why not try {game["name"]}?  {game["link"]}')


client.run(token)
